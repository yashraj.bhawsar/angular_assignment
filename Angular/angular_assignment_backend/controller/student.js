const pool = require("../config/dbConnection");

exports.getStudents = (_req, res) => {
    pool.query("select * from students", function (err, data) {
        if (err) {
            res.json({ success: false, error: err })
        } else {
            res.json({ success: true, data: data });
        }
    })
}

exports.addStudent = (req, res)=>{
    let {studentName, studentRollNo, dob, physics, maths, chemistry} = req.body;

    const query = "insert into students (studentName, studentRollNo, dob, physics, maths, chemistry) values(?,?,?,?,?,?)";
    pool.query(query, [studentName, studentRollNo, dob, physics, maths, chemistry], function(err){
        if(err){
            console.log(err);
            res.json({success: false});
        }else{
            res.json({success: true})
        }
    })
}

exports.studentLogin = (req, res) => {
    let studentRollNo = req.body.rollNo;
    let dob = req.body.dob;

    const query = "select * from students where studentRollNo = ? and dob = ?"
    pool.query(query, [studentRollNo, dob], function (err, data) {
        if (err) {
            res.json({ success: false, error: err })
        }
        else {
            if(!data.length){
                res.json({ success: false, error: "invalid roll no or date of birth" });
            }else{
                res.json({ success: true, data: data });
            }
        }
    })
}

exports.deleteStudent = (req, res)=>{
    const studentId = req.body.studentId; 
    console.log(req.body);
    console.log(studentId);
    const query = "delete from students where studentId = ?";
    pool.query(query, [studentId], function (err) {
        if (err) {
            console.log(err);
            res.json({ success: false, error: err })
        }
        else {
            res.json({ success: true });
        }
    })
}

exports.editStudent = (req, res)=>{
    console.log(req.body);
    const {studentId, studentRollNo, studentName, dob, physics, maths, chemistry} = req.body;
    
    const query = "update students set studentRollNo = ?, studentName = ?, dob = ?, physics= ?, maths = ?, chemistry = ?  where studentId = ?";
    pool.query(query, [studentRollNo, studentName, dob, physics, maths, chemistry, studentId], function(err, data){
        if(err){
            console.log(err);
            res.json({success: false, error: err})
        }else{
            res.json({success: true})
        }
    })
}