const pool = require("../config/dbConnection");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.teacherLogin = (req, res) => {
    let { userName, password } = req.body;
    const query = "select * from teacher where userName = ?";
    pool.query(query, [userName], function (err, data) {
        if (data.length) {
            const isMatch = bcrypt.compareSync(password, data[0].password)
            const token = jwt.sign(
                { teacherId: data[0].teacherId },
                process.env.JWT_SECRET_KEY,
                { expiresIn: "5d" }
            );
            if (isMatch)
                res.status(200).json({ success: true, data: data, token });
            else
                res.json({ success: false, error: "invalid password" });
        }
        else if (err) {
            console.log(err)
            res.json({ success: false, error: err })
        }
        else {
            res.json({ success: false, error: "invalid email" });
        }
    })
}

exports.saveTeacher = async(req, res) => {
    const { teacherName, userName, password } = req.body;
    try {
        const hashPassword = await bcrypt.hash(password, 10);
        const query = "insert into teacher ( teacherName , userName , password ) values(?,?,?)";
        pool.query(query, [teacherName, userName, hashPassword], function (err) {
            if (err) {
                console.log("error : " + err);
                res.status(500).json({ success: false });
            }
            else {
                res.status(200).json({ success: true });
            }
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false });
    }
}
