const jwt = require("jsonwebtoken");
let pool = require("../config/dbConnection");

let checkUserAuth = async (req, res, next) => {
  let token;
  const { authorization } = req.headers;
  if (authorization && authorization.startsWith("Bearer")) {
    try {
      token = authorization.split(" ")[1];
      console.log(token);
      const { teacherId } = jwt.verify(token, process.env.JWT_SECRET_KEY);
      console.log(teacherId);
      const query = "select * from teacher where teacherId = ?";
      pool.query(query, [teacherId], function(error, data){
          if(error){
            console.log(error);
            res.status(401).send({ success: false, message: "unauthorized user" });
          }else{
            req.user = data[0];
            next();
          }
      })
    } catch (error) {
      console.log(error);
      res.status(401).send({ success: false, message: "unauthorized user" });
    }
  }
  if (!token) {
    res
      .status(401)
      .send({ success: false, message: "unauthorized user, no toekn" });
  }
};

module.exports = checkUserAuth;