let express = require('express');
let router = express.Router();
const auth = require("../middleware/auth")
const Controller = require("../controller/student")


router.get('/getStudents',auth, Controller.getStudents);

router.post("/studentLogin", Controller.studentLogin);

router.post("/addStudent",auth,  Controller.addStudent);

router.post("/deleteStudent", auth, Controller.deleteStudent);

router.post("/editStudent", auth, Controller.editStudent);

module.exports = router;
