import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {Router} from '@angular/router'; 

import { TeacherService } from "../service/teacher.service"

@Component({
  selector: 'app-teacher-login',
  templateUrl: './teacher-login.component.html',
  styleUrls: ['./teacher-login.component.css']
})
export class TeacherLoginComponent implements OnInit {

  hide: boolean
  teacher: any

  constructor(private teacherData: TeacherService, private route:Router) {
    this.hide = true;
  }

  ngOnInit(): void {
  }

  getTeacherData(teacherData: any) {
    if (teacherData.userName && teacherData.password) {
      this.teacherData.teachers(teacherData).subscribe((data) => {
        this.teacher = data;
        if (this.teacher.success) {
          localStorage.setItem("token", this.teacher.token)
          this.teacher = this.teacher.data[0];
          sessionStorage.setItem("teacherInfo", JSON.stringify(this.teacher))
          Swal.fire("success", "Login Successfull", "success");
          this.route.navigate(['/dashboard']);
        } else {
          Swal.fire("Error", this.teacher.error, "error");
        }
      })
    }
  }

}
