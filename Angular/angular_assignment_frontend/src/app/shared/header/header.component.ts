import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'; 

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  teacher : any
  student : any

  constructor(private route:Router) { }

  ngOnInit(): void {
    if( sessionStorage.getItem("teacherInfo"))
      this.teacher = JSON.parse( sessionStorage.getItem("teacherInfo") + "" );
    this.student = JSON.parse( sessionStorage.getItem("studentInfo") + "");
  }

  handleLogOut(){
    sessionStorage.setItem("teacherInfo", "");
    localStorage.removeItem("token");
    this.route.navigate(['/']);
  }

}
