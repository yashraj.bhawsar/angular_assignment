import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

import { StudentService } from "../service/student.service";

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css']
})
export class StudentLoginComponent implements OnInit {

  hide: boolean
  students: any

  constructor(private studentData: StudentService, private route:Router) {
    this.hide = true;
  }

  ngOnInit(): void {

  }

  getStudentData(studentData: any) {
    if (studentData.rollNo && studentData.dob) {
      this.studentData.students(studentData).subscribe((data) => {
        this.students = data;
        if (this.students.success) {
          this.students = this.students.data[0];
          sessionStorage.setItem("studentInfo", JSON.stringify(this.students));
          Swal.fire("success", "Login Successfull", "success");
          this.route.navigate(['/result']);
        } else {
          Swal.fire("Error", this.students.error, "error");
        }
      });
    }
  }

}
