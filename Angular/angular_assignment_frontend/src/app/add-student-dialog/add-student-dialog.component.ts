import { Component, OnInit, Inject } from '@angular/core';
import Swal from 'sweetalert2';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-add-student-dialog',
  templateUrl: './add-student-dialog.component.html',
  styleUrls: ['./add-student-dialog.component.css']
})
export class AddStudentDialogComponent implements OnInit {

  result: any
  students: any

  constructor(private studentService: StudentService, 
    public dialogRef: MatDialogRef<AddStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
    }

  ngOnInit(): void {
  }

  addStudent(studentData: any) {
    if (studentData.studentRollNo && studentData.studentName && studentData.dob && studentData.physics && studentData.maths && studentData.chemistry) {
      this.studentService.createStudent(studentData).subscribe((data) => {
        this.result = data;
        if (this.result.success) {
          Swal.fire("Added", "Student added successfully", "success");
        } else {
          Swal.fire("Error", "OOPS! server error", "error");
        }
        this.dialogRef.close();
      })
    }
  }
}
