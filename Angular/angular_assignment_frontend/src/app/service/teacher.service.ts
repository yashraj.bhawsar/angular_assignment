import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

const serverURL = "http://localhost:5000/teacher/";

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http : HttpClient) { }

  teachers(teacherInfo : any){
    return this.http.post(serverURL + "teacherLogin", teacherInfo);
  }
}
