import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"

const serverURL = "http://localhost:5000/student/";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  header : any

  constructor(private http : HttpClient) {
    this.header = new HttpHeaders().set(
      "Authorization",
       "Bearer " + localStorage.getItem("token")
    );
  }

  students(studentInfo : any){
    return this.http.post(serverURL + "studentLogin", studentInfo , {headers: this.header});
  }

  getAllStudents(){
    return this.http.get(serverURL + "getStudents", {headers: this.header});
  }

  createStudent(studentData : any){
    return this.http.post(serverURL + "addStudent", studentData, {headers: this.header});
  }

  deleteStudent(studentId : number){
    return this.http.post(serverURL + "deleteStudent", studentId, {headers: this.header});
  }

  editStudent(studentData : any, studentId: number){
    studentData.studentId = studentId;
    return this.http.post(serverURL + "editStudent", studentData, {headers: this.header});
  }
}
