import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentLoginComponent } from './student-login/student-login.component';
import { TeacherDashboardComponent} from "./teacher-dashboard/teacher-dashboard.component";
import { ViewResultComponent } from './view-result/view-result.component';

const routes: Routes = [
  {
    component: StudentLoginComponent,
    path: ""
  },
  {
    component: TeacherDashboardComponent,
    path: "dashboard"
  },
  {
    component: ViewResultComponent,
    path: "result"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
